//
//  CollectionViewCell.swift
//  
//
//  Created by Sierra 4 on 23/02/17.
//
//

import UIKit

class CollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource{
    
    var cellData:[Details]?
    
    
    @IBOutlet var tbleViewoutlet: UITableView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        if(tbleViewoutlet == nil) {
            return
        }
        tbleViewoutlet.delegate = self
        tbleViewoutlet.dataSource = self
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return cellData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
      let cell:TableViewCell = (tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as? TableViewCell)!
        
        cell.subjectViewOutlet.text = cellData?[indexPath.row].subjects?[0].subject_name
        cell.enrolledviewOutlet.text = String(describing: cellData?[indexPath.row].enrolled ?? 0)
        cell.ageViewOutlet.text = cellData?[indexPath.row].age_group
        cell.perStudentViewOutlet.text = (describing: cellData?[indexPath.row].charge_student)
        cell.timeViewOutlet.text = cellData?[indexPath.row].time_duration
        cell.timeCellOutlet.text = cellData?[indexPath.row].start_time1
       // if IndexPath.row == cellData.count - 1 {
          //  cell.lineUnderDot.backgroudColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        //}
        
        return TableViewCell()
    }

   func numberOfSections(in tableView: UITableView) -> Int
   {
        return 1
   
    }

}
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
