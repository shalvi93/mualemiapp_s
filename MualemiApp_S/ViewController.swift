//
//  ViewController.swift
//  MualemiApp_S
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SVProgressHUD


class ViewController: UIViewController {
    
    var userModel: Instructor?
    var idata : Instructor?
    var cellData:[Details]?
    
    @IBOutlet var imageDay: [UIImageView]!
    @IBOutlet weak var TodayLabel: UILabel!
    @IBOutlet weak var colVwOutlet: UICollectionView!
    
    @IBOutlet weak var monLabel: UILabel!
    
    @IBOutlet weak var tueLabel: UILabel!
    
    @IBOutlet weak var wedLabel: UILabel!
    
    @IBOutlet weak var ThusLabel: UILabel!
    
    @IBOutlet weak var friLabel: UILabel!
    
    @IBOutlet weak var satLabel: UILabel!
    
    @IBOutlet weak var sunLabel: UILabel!
    
    var Presentdate = " "
    let formatter = DateFormatter()
    var result : String?
    var subjectArray : [String]?
    var txt : String = ""
    var temp : String = ""
    let date = Date()
    
   
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
    
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        Presentdate = formatter.string(from: date)
        fetchData()
       
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
    }
    
    func getDayOfWeekString(today:String)->String?
    {
        if let todayDate = formatter.date(from: today)
        {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            
            switch (weekDay!)
            {
            case 1:
                return "Sun"
            case 2:
                return "Mon"
            case 3:
                return "Tues"
            case 4:
                return "Wed"
            case 5:
                return "Thu"
            case 6:
                return "Fri"
            case 7:
                return "Sat"
            default:
                return "Nil"
            }
        }
        return nil
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = colVwOutlet.contentOffset
        visibleRect.size = colVwOutlet.bounds.size
    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
    let visibleIndexPath: IndexPath = colVwOutlet.indexPathForItem(at: visiblePoint)!
        
        let lastElement = (userModel?.idata?.count)! - 1
        if visibleIndexPath.row == lastElement {
            Alamofire.request("http://34.195.206.185/api/instructor-home")
            let param:[String:Any] = ["access_token":"ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8", "page_no":"1","page_size":"7","last_date":2017-03-02,"date_selected": Presentdate]
            ApiHandler.fetchData(urlStr: "instructor-home", parameters: param) { (jsonData) in

                
                let userModel2 = Mapper<Instructor>().map(JSONObject: jsonData)
                
                for temp in 0..<7 {
                    self.userModel?.idata?.append((userModel2?.idata?[temp])!)
                }
                self.colVwOutlet.reloadData()
            }
        }
    }
    
    
    func fetchData()
    {
        formatter.dateFormat = "yyyy-MM-dd"
        let param:[String:Any] = ["access_token" : "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8" , "page_no": "1" ,"page_size" : "7" ,"date_selected" :Presentdate]
        
        ApiHandler.fetchData(urlStr: "instructor-home", parameters: param) {
            (jsonData) in
            self.userModel = Mapper<Instructor>().map(JSONObject: jsonData)
            
            print(self.userModel?.msg ?? "")
            print(jsonData!)
            self.colVwOutlet.reloadData()
            
            self.monLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[0].date1!)!)
            self.tueLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[1].date1!)!)
            self.wedLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[2].date1!)!)
           self.ThusLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[3].date1!)!)
            self.friLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[4].date1!)!)
            self.satLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[5].date1!)!)
            self.sunLabel.text = self.getDayOfWeekString(today:(self.userModel?.idata?[6].date1!)!)
          self.TodayLabel.text = ("Today, \(self.userModel?.idata?[0].date ?? "")")
            print(self.userModel?.msg ?? "")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let selectedIndex = indexPath.row
        self.FieldsReset()
        self.imageDay[ selectedIndex % 7 ].image = UIImage(named: "Eject")
        
    }
    
    func FieldsReset() {
        for index in 0..<7 {
            imageDay[index].image = UIImage(named: "")
        }
    }
}

extension ViewController:UICollectionViewDataSource,UICollectionViewDelegate
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.userModel?.idata?.count) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell:CollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? CollectionViewCell)
            else{
                return CollectionViewCell()
        }
        SVProgressHUD.dismiss()
        cell.cellData = (self.userModel?.idata?[indexPath.row].details)!
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        self.colVwOutlet.isPagingEnabled = true
        let myWidth = collectionView.frame.width
        let myHeight = collectionView.frame.height
        return CGSize(width: myWidth, height:myHeight)
    }
}

